package com.example.practicas_u2

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.practicas_u2.practica1.Practica1Activity
import com.example.practicas_u2.practica2.Practica2Activity
import com.example.practicas_u2.practica3.Practica3Activity
import com.example.practicas_u2.practica4.Practica4Activity
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnPractica1.setOnClickListener {
            val intent = Intent(this, Practica1Activity::class.java)
            startActivity(intent)

        }
        btnPractica2.setOnClickListener {
            val intent = Intent(this, Practica2Activity::class.java)
            startActivity(intent)

        }

        btnPractica3.setOnClickListener {
            val intent = Intent(this, Practica3Activity::class.java)
            startActivity(intent)

        }

        btnPractica4.setOnClickListener {
            val intent = Intent(this, Practica4Activity::class.java)
            startActivity(intent)
        }
    }
}