package com.example.practicas_u2.practica2

import android.app.Activity
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.practicas_u2.R
import kotlinx.android.synthetic.main.activity_duration_selector.*

class DurationSelectorActivity : AppCompatActivity() {

    var durationList = mutableListOf<Int>()

    private val adapter by lazy {
        DurationAdapter{ duration ->
            intent.putExtra( "DURATION", duration)
            setResult(Activity.RESULT_OK, intent)
            finish()
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_duration_selector)
        durationList = mutableListOf(5, 10, 15, 20, 25, 30, 60, 120, 180, 240)
        rvDuration.adapter = adapter
        adapter.setList(durationList)

    }

}