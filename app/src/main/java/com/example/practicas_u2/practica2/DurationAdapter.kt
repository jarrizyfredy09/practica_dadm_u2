package com.example.practicas_u2.practica2

import android.view.LayoutInflater
import android.view.OrientationEventListener
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.practicas_u2.R
import kotlinx.android.synthetic.main.item_list.view.*

class DurationAdapter(private val listener: (Int) -> Unit) :
    RecyclerView.Adapter<DurationAdapterViewHolder>() {

    private var List = mutableListOf<Int>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DurationAdapterViewHolder {
        val itemview =
            LayoutInflater.from(parent.context).inflate(R.layout.item_list, parent, false)
        return DurationAdapterViewHolder(itemview)
    }

    override fun onBindViewHolder(holder: DurationAdapterViewHolder, position: Int) {
        holder.setData(List[position], listener)
    }

    override fun getItemCount(): Int {
        return List.size
    }

    fun setList(list: List<Int>) {
        this.List.addAll(list)
        notifyDataSetChanged()
    }

}

class DurationAdapterViewHolder(itemview: View) : RecyclerView.ViewHolder(itemview) {

    fun setData(duration: Int, listener: (Int) -> Unit) {

        itemView.apply {
            var horas: Int = 0
            if (duration / 60 > 0) {
                horas - duration / 60
                tvMinutes.text = context.resources.getQuantityString(R.plurals.pluralsHours, horas, horas)
            } else {
                tvMinutes.text = "$duration minutes"
            }

            setOnClickListener { listener.invoke(duration) }

        }


    }

}