package com.example.practicas_u2.practica4

import android.content.DialogInterface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.example.practicas_u2.R
import kotlinx.android.synthetic.main.activity_practica4.*

class Practica4Activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_practica4)

        btnAd.setOnClickListener {
            val builder: AlertDialog.Builder = AlertDialog.Builder(this)
            builder.setMessage("Esto es un cuadro de dialogo")
                .setTitle("HOLA")
                //.setCancelable(false) // Para evitar que ssalga del cuadro de dialogo
                .setPositiveButton("SIMON") { dialog, which ->
                    Toast.makeText(this, "Amonos", Toast.LENGTH_SHORT).show()
                }
                .setNeutralButton("No lo se") { dialog, which ->
                    Toast.makeText(this, "Meh", Toast.LENGTH_SHORT).show()
                }
                .setNegativeButton("NELSON") { dialog, which ->
                    Toast.makeText(this, "Ihh", Toast.LENGTH_SHORT).show()
                }


            val dialog: AlertDialog = builder.create()
            dialog.show()
        }

        btnAd2.setOnClickListener {
            val colors = arrayOf("Red", "Blue", "Green", "Yellow")
            val builder: AlertDialog.Builder = AlertDialog.Builder(this)
                .setTitle("Escoje un color")
                .setItems(colors) { dialog, which ->
                    //which contiene el index del item seleccionado
                    when (which) {
                        0 -> Toast.makeText(this, "Red", Toast.LENGTH_SHORT).show()
                        1 -> Toast.makeText(this, "Blue", Toast.LENGTH_SHORT).show()
                        2 -> Toast.makeText(this, "Green", Toast.LENGTH_SHORT).show()
                        3 -> Toast.makeText(this, "Yellow", Toast.LENGTH_SHORT).show()
                    }
                }
            val dialog = builder.create()
            dialog.show()
        }
    }
}