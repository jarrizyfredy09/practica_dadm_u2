package com.example.practicas_u2.practica3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.practicas_u2.R
import kotlinx.android.synthetic.main.activity_p3.*

class P3Activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_p3)

        var bundle = Bundle()
        bundle = intent.extras ?: Bundle()
        val name = bundle.getString("NAME")
        val lastname = bundle.getString("LASTNAME")
        val age = bundle.getInt("AGE")
        val salary = bundle.getInt("SALARY")
        val gender = bundle.getString("GENDER")
        tvInfo.text = String.format(getString(R.string.info), name, lastname, age, salary, gender)
    }
}